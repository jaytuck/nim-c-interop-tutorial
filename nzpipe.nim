import os

const
  DEBUG = true
  # zlib constants

  # Allowed flush values; see deflate() and inflate() below for details
  Z_NO_FLUSH      = 0
  Z_PARTIAL_FLUSH = 1
  Z_SYNC_FLUSH    = 2
  Z_FULL_FLUSH    = 3
  Z_FINISH        = 4
  Z_BLOCK         = 5
  Z_TREES         = 6

  # Return codes for the compression/decompression functions. Negative values
  # are errors, positive values are used for special but normal events.
  Z_OK            = 0
  Z_STREAM_END    = 1
  Z_NEED_DICT     = 2
  Z_ERRNO        = (-1)
  Z_STREAM_ERROR = (-2)
  Z_DATA_ERROR   = (-3)
  Z_MEM_ERROR    = (-4)
  Z_BUF_ERROR    = (-5)
  Z_VERSION_ERROR = (-6)

  # compression levels
  Z_NO_COMPRESSION         = 0
  Z_BEST_SPEED             = 1
  Z_BEST_COMPRESSION       = 9
  Z_DEFAULT_COMPRESSION  = (-1)

proc def(source: File, dest: File, level: cint): cint =
  var
    ret, flush: cint

  if DEBUG:
    stderr.writeLine("started running def function")


# compress or decompress from stdin to stdout
var ret: cint
let argv = commandLineParams()

# do compression if no arguments
if argv.len == 0:
  if DEBUG:
    stderr.writeLine("starting compression")
  ret = def(stdin, stdout, Z_DEFAULT_COMPRESSION)
  quit(ret)

# do decompression if -d specified
elif argv.len == 1 and argv[0] == "-d":
  if DEBUG:
    stderr.writeLine("starting decompression")
  # TODO

# otherwise, report usage
else:
  stderr.writeLine("nzpipe usage: zpipe [-d] < source > dest")
  quit(1)
